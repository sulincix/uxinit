#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define PATH_MAX 1024

int file_exists(const char *fname){
    FILE *file;
    if ((file = fopen(fname, "r"))) {
        fclose(file);
        return 1;
    }
}

int find_dp(){
    int dp = 0;
    for(int i=0; i >-1;i++){
         char *lock= malloc(sizeof(char)*PATH_MAX);
         strcpy(lock, "/tmp/.X");
         char* str;
         asprintf (&str, "%i", i);
         strcat(lock, str);
         strcat(lock, "-lock");
         free(str);
         if(!file_exists(lock)){
            dp = i;
            break;
        }
    }
    return dp;
}

void remove_lock(int dp){
    char *lock= malloc(sizeof(char)*PATH_MAX);
    strcpy(lock, "/tmp/.X");
    char* str;
    asprintf (&str, "%i", dp);
    strcat(lock, str);
    strcat(lock, "-lock");
    remove(lock);
    free(lock);
}
