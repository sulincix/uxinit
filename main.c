#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define PATH_MAX 1024
// Header begin
int file_exists(const char *fname);
int find_dp();
void remove_lock(int dp);

// Decleration
void waitpid(pid_t pid);

pid_t sv_pid = -1;
pid_t cl_pid = -1;
int DISPLAY = -1;


void kill_xorg(int sig){
    remove_lock(DISPLAY);
    kill(sv_pid, SIGKILL);
    kill(cl_pid, SIGKILL);
}

int main(int argc, char* argv[]){
    // connect signal
    signal(SIGINT, kill_xorg);
    // find dp
    DISPLAY = find_dp();
    char* dp;
    asprintf (&dp, ":%i", DISPLAY);
    // Go home
    char* home = getenv("HOME");
    if(home != NULL){
        chdir(home);
    }
    // create new X window
    sv_pid = fork();
    if(sv_pid == 0){
        char* args[] = {"X", "-nolisten", "tcp", "-pixmap32", dp, NULL};
        execvp("/usr/bin/X", args);
        return 0;
    }
    // set DISPLAY
    setenv("DISPLAY", dp, 1);
    // allox local connctions
    system("xhost +local:");
    // start xinitrc
    cl_pid = fork();
    if(cl_pid == 0){
        char* xinit_args[] = {"sh", "/etc/X11/xinit/xinitrc", NULL};
        execvp("/bin/sh", xinit_args);
        return 0;
    }
    waitpid(cl_pid);
    kill_xorg(SIGKILL);
    //cleanup and exit
    free(dp);
    return 0;
}
